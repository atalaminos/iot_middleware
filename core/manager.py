#!/usr/bin/env python
# -*- coding: utf-8 -*-
import mysql.connector
from utils import Utils

class Manager:

    @staticmethod
    def get_publishers_info(data):
        result = dict()
        result['total_publishers'] = 0
        result['total_mqtt_publishers'] = 0
        result['total_coap_publishers'] = 0
        result['total_samples_sent'] = 0
        result['total_mqtt_samples_sent'] = 0
        result['total_coap_samples_sent'] = 0

        publishers = dict()
        total_mqtt_publishers = dict()
        total_coap_publishers = dict()
        for row in data:

            id = row['ip'] + str(row['pid']) + str(row['process_id'])
            publishers[id] = 1

            result['total_samples_sent'] += 1

            if row['protocol'] == 'MQTT':
                result['total_mqtt_samples_sent'] += 1
                total_mqtt_publishers[id] = 1

            elif row['protocol'] == 'COAP':
                result['total_mqtt_samples_sent'] += 1
                total_coap_publishers[id] = 1

        result['total_publishers'] = len(publishers)
        result['total_mqtt_publishers'] = len(total_mqtt_publishers)
        result['total_coap_publishers'] = len(total_coap_publishers)

        return result

    @staticmethod
    def get_subscribers_info(data):
        result = dict()
        result['total_subscribers'] = 0
        result['total_mqtt_subscribers'] = 0
        result['total_coap_subscribers'] = 0
        result['total_samples_received'] = 0
        result['total_mqtt_samples_received'] = 0
        result['total_coap_samples_received'] = 0
        result['total_mqtt_samples_received_by_mqtt_subscribers'] = 0
        result['total_mqtt_samples_received_by_coap_subscribers'] = 0
        result['total_coap_samples_received_by_coap_subscribers'] = 0
        result['total_coap_samples_received_by_mqtt_subscribers'] = 0

        subscribers = dict()
        total_mqtt_subscribers = dict()
        total_coap_subscribers = dict()
        for row in data:

            id = row['ip_subscriber'] + str(row['pid_subscriber']) + str(row['process_id_subscriber'])
            subscribers[id] = 1

            result['total_samples_received'] +=1

            if row['protocol'] == 'MQTT':

                result['total_mqtt_samples_received'] += 1

                if row['protocol_subscriber'] == 'MQTT':
                    result['total_mqtt_samples_received_by_mqtt_subscribers'] += 1
                    total_mqtt_subscribers[id] = 1
                    
                elif row['protocol_subscriber'] == 'COAP':
                    result['total_mqtt_samples_received_by_coap_subscribers'] += 1
                    total_coap_subscribers[id] = 1

            elif row['protocol'] == 'COAP':

                result['total_coap_samples_received'] += 1

                if row['protocol_subscriber'] == 'MQTT':
                    result['total_coap_samples_received_by_mqtt_subscribers'] += 1
                    total_mqtt_subscribers[id] = 1
                    
                elif row['protocol_subscriber'] == 'COAP':
                    result['total_coap_samples_received_by_coap_subscribers'] += 1
                    total_coap_subscribers[id] = 1

        result['total_subscribers'] = len(subscribers)
        result['total_mqtt_subscribers'] = len(total_mqtt_subscribers)
        result['total_coap_subscribers'] = len(total_coap_subscribers)

        return result

    @staticmethod
    def latency_jitter(data):

        info = dict()

        info['total_mean_latency'] = 0.0
        info['min_latency'] = 0.0
        info['max_latency'] = 0.0
        info['jitter'] = 0.0

        info['mean_latency_mqtt'] = 0.0
        info['min_latency_mqtt'] = 0.0
        info['max_latency_mqtt'] = 0.0
        info['jitter_mqtt'] = 0.0

        info['mean_latency_coap'] = 0.0
        info['min_latency_coap'] = 0.0
        info['max_latency_coap'] = 0.0
        info['jitter_coap'] = 0.0

        info['mean_latency_mqtt_from_mqtt'] = 0.0
        info['min_latency_mqtt_from_mqtt'] = 0.0
        info['max_latency_mqtt_from_mqtt'] = 0.0
        info['jitter_latency_mqtt_from_mqtt'] = 0.0

        info['mean_latency_mqtt_from_coap'] = 0.0
        info['min_latency_mqtt_from_coap'] = 0.0
        info['max_latency_mqtt_from_coap'] = 0.0
        info['jitter_latency_mqtt_from_coap'] = 0.0

        info['mean_latency_coap_from_coap'] = 0.0
        info['min_latency_coap_from_coap'] = 0.0
        info['max_latency_coap_from_coap'] = 0.0
        info['jitter_latency_coap_from_coap'] = 0.0

        info['mean_latency_coap_from_mqtt'] = 0.0
        info['min_latency_coap_from_mqtt'] = 0.0
        info['max_latency_coap_from_mqtt'] = 0.0
        info['jitter_latency_coap_from_mqtt'] = 0.0

        latency = list()
        latency_mqtt = list()
        latency_coap = list()
        latency_mqtt_from_mqtt = list()
        latency_mqtt_from_coap = list()
        latency_coap_from_coap = list()
        latency_coap_from_mqtt = list()

        for row in data:

            latency.append(row['latency'])

            if info['min_latency'] == 0.0 or info['min_latency'] > row['latency']:
                info['min_latency'] = row['latency']

            if info['max_latency'] == 0.0 or info['max_latency'] < row['latency']:
                info['max_latency'] = row['latency']

            if row['protocol'] == 'MQTT':

                latency_mqtt.append(row['latency'])

                if info['min_latency_mqtt'] == 0.0 or info['min_latency_mqtt'] > row['latency']:
                    info['min_latency_mqtt'] = row['latency']

                if info['max_latency_mqtt'] == 0.0 or info['max_latency_mqtt'] < row['latency']:
                    info['max_latency_mqtt'] = row['latency']

                if row['protocol_subscriber'] == 'MQTT':

                    latency_mqtt_from_mqtt.append(row['latency'])

                    if info['min_latency_mqtt_from_mqtt'] == 0.0 or info['min_latency_mqtt_from_mqtt'] > row['latency']:
                        info['min_latency_mqtt_from_mqtt'] = row['latency']

                    if info['max_latency_mqtt_from_mqtt'] == 0.0 or info['max_latency_mqtt_from_mqtt'] < row['latency']:
                        info['max_latency_mqtt_from_mqtt'] = row['latency']

                elif row['protocol_subscriber'] == 'COAP':

                    latency_coap_from_mqtt.append(row['latency'])

                    if info['min_latency_coap_from_mqtt'] == 0.0 or info['min_latency_coap_from_mqtt'] > row['latency']:
                        info['min_latency_coap_from_mqtt'] = row['latency']

                    if info['max_latency_coap_from_mqtt'] == 0.0 or info['max_latency_coap_from_mqtt'] < row['latency']:
                        info['max_latency_coap_from_mqtt'] = row['latency']

            elif row['protocol'] == 'COAP':

                latency_coap.append(row['latency'])

                if info['min_latency_coap'] == 0.0 or info['min_latency_coap'] > row['latency']:
                    info['min_latency_coap'] = row['latency']

                if info['max_latency_coap'] == 0.0 or info['max_latency_coap'] < row['latency']:
                    info['max_latency_coap'] = row['latency']

                if row['protocol_subscriber'] == 'MQTT':

                    latency_mqtt_from_coap.append(row['latency'])

                    if info['min_latency_mqtt_from_coap'] == 0.0 or info['min_latency_mqtt_from_coap'] > row['latency']:
                        info['min_latency_mqtt_from_coap'] = row['latency']

                    if info['max_latency_mqtt_from_coap'] == 0.0 or info['max_latency_mqtt_from_coap'] < row['latency']:
                        info['max_latency_mqtt_from_coap'] = row['latency']

                elif row['protocol_subscriber'] == 'COAP':

                    latency_coap_from_coap.append(row['latency'])

                    if info['min_latency_coap_from_coap'] == 0.0 or info['min_latency_coap_from_coap'] > row['latency']:
                        info['min_latency_coap_from_coap'] = row['latency']

                    if info['max_latency_coap_from_coap'] == 0.0 or info['max_latency_coap_from_coap'] < row['latency']:
                        info['max_latency_coap_from_coap'] = row['latency']

        utils = Utils()

        info['total_mean_latency'] = utils.mean(latency)
        info['jitter'] = utils.pstdev(latency)

        info['mean_latency_mqtt'] = utils.mean(latency_mqtt)
        info['jitter_mqtt'] = utils.pstdev(latency_mqtt)

        info['mean_latency_coap'] = utils.mean(latency_coap)
        info['jitter_coap'] = utils.pstdev(latency_coap)

        info['mean_latency_mqtt_from_mqtt'] = utils.mean(latency_mqtt_from_mqtt)
        info['jitter_latency_mqtt_from_mqtt'] = utils.pstdev(latency_mqtt_from_mqtt)

        info['mean_latency_mqtt_from_coap'] = utils.mean(latency_mqtt_from_coap)
        info['jitter_latency_mqtt_from_coap'] = utils.pstdev(latency_mqtt_from_coap)

        info['mean_latency_coap_from_coap'] = utils.mean(latency_coap_from_coap)
        info['jitter_latency_coap_from_coap'] = utils.pstdev(latency_coap_from_coap)

        info['mean_latency_coap_from_mqtt'] = utils.mean(latency_coap_from_mqtt)
        info['jitter_latency_coap_from_mqtt'] = utils.pstdev(latency_coap_from_mqtt)

        return info


    @staticmethod
    def lost_samples(publications_data, suscriptions_data):

        manager = Manager()
        utils = Utils()

        subscribers_info = manager.get_subscribers_info(suscriptions_data)
        suscriptions_data_dict = utils.convert_list_to_dict(suscriptions_data)

        info = dict()
        info['lost_samples'] = 0
        info['lost_samples_mqtt'] = 0
        info['lost_samples_coap'] = 0
        info['lost_samples_mqtt_from_mqtt'] = 0
        info['lost_samples_mqtt_from_coap'] = 0
        info['lost_samples_coap_from_coap'] = 0
        info['lost_samples_coap_from_mqtt'] = 0

        for row in publications_data:

            msg = row['msg']

            # Un publicador ha enviado un mensaje que no ha sido recibido por nadie
            if msg not in suscriptions_data_dict:

                info['lost_samples'] += subscribers_info['total_subscribers']

                if row['protocol'] == 'MQTT':
                    
                    info['lost_samples_mqtt'] += subscribers_info['total_subscribers']
                    info['lost_samples_mqtt_from_mqtt'] += subscribers_info['total_mqtt_subscribers']
                    info['lost_samples_coap_from_mqtt'] += subscribers_info['total_coap_subscribers']

                elif row['protocol'] == 'COAP':

                    info['lost_samples_coap'] += subscribers_info['total_subscribers']
                    info['lost_samples_coap_from_coap'] += subscribers_info['total_coap_subscribers']
                    info['lost_samples_mqtt_from_coap'] += subscribers_info['total_mqtt_subscribers']

            # Al menos un suscriptor recibió el mensaje
            else:

                received_by = 0
                received_by_mqtt = 0
                received_by_coap = 0

                for data in suscriptions_data_dict[msg]:

                    received_by +=1

                    if data['protocol'] == 'MQTT':             
                        received_by_mqtt += 1
                        
                    elif data['protocol'] == 'COAP':
                        received_by_coap += 1

                if received_by != subscribers_info['total_subscribers']:

                    info['lost_samples'] += subscribers_info['total_subscribers'] - received_by

                    if row['protocol'] == 'MQTT':

                        info['lost_samples_mqtt'] += subscribers_info['total_subscribers'] - received_by
                        info['lost_samples_mqtt_from_mqtt'] += subscribers_info['total_mqtt_subscribers'] - received_by_mqtt
                        info['lost_samples_coap_from_mqtt'] += subscribers_info['total_coap_subscribers'] - received_by_coap

                    elif row['protocol'] == 'COAP':

                        info['lost_samples_coap'] += subscribers_info['total_subscribers'] - received_by
                        info['lost_samples_mqtt_from_coap'] += subscribers_info['total_mqtt_subscribers'] - received_by_coap
                        info['lost_samples_coap_from_coap'] += subscribers_info['total_coap_subscribers'] - received_by_coap

        return info
