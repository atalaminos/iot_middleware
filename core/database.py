#!/usr/bin/env python
# -*- coding: utf-8 -*-
import mysql.connector


class Mysql:

    cnx = None
    cursor = None

    publisher_fields = None
    subscribers_fields = None
    traffic_fields = None

    sql_show_publications = "SHOW TABLES LIKE 'publications'"
    sql_show_subscriptions = "SHOW TABLES LIKE 'subscriptions'"
    sql_show_traffic = "SHOW TABLES LIKE 'traffic'"

    sql_get_count_publications = "SELECT COUNT(*) from publications"
    sql_get_count_subscriptions = "SELECT COUNT(*) from subscriptions"
    sql_get_count_traffic = "SELECT COUNT(*) from traffic"

    sql_save_publication = "INSERT INTO publications (num, ip, pid, process_id, protocol, time, msg) " \
                           "VALUES (%s, %s, %s, %s, %s, %s, %s)"
    sql_save_subscription = "INSERT INTO subscriptions (num, ip, pid, process_id, protocol, time, msg, " \
                           "ip_subscriber, pid_subscriber, process_id_subscriber, protocol_subscriber, " \
                           "time_subscriber, latency) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    sql_save_traffic = "INSERT INTO traffic (received, received_mqtt, received_coap) VALUES (%s,%s,%s)"

    sql_get_all_publications = "SELECT * from publications"
    sql_get_all_suscriptions = "SELECT * from subscriptions"
    sql_get_all_traffic = "SELECT * from traffic"

    sql_truncate_publications = "TRUNCATE publications"
    sql_truncate_subscriptions = "TRUNCATE subscriptions"
    sql_truncate_traffic = "TRUNCATE traffic"

    sql_drop_publications = "DROP TABLE publications"
    sql_drop_subscriptions = "DROP TABLE subscriptions"
    sql_drop_traffic = "DROP TABLE traffic"

    def __init__(self, host, user, password, database):
        self.cnx = mysql.connector.connect(user=user, password=password, host=host, database=database,
                                           charset='utf8', use_unicode=True)

        self.cursor = self.cnx.cursor()

        self.publisher_fields = list()
        self.publisher_fields.append('id')
        self.publisher_fields.append('num')
        self.publisher_fields.append('ip')
        self.publisher_fields.append('pid')
        self.publisher_fields.append('process_id')
        self.publisher_fields.append('protocol')
        self.publisher_fields.append('time')
        self.publisher_fields.append('msg')

        self.subscribers_fields = list()
        self.subscribers_fields.append('id')
        self.subscribers_fields.append('num')
        self.subscribers_fields.append('ip')
        self.subscribers_fields.append('pid')
        self.subscribers_fields.append('process_id')
        self.subscribers_fields.append('protocol')
        self.subscribers_fields.append('time')
        self.subscribers_fields.append('msg')
        self.subscribers_fields.append('ip_subscriber')
        self.subscribers_fields.append('pid_subscriber')
        self.subscribers_fields.append('process_id_subscriber')
        self.subscribers_fields.append('protocol_subscriber')
        self.subscribers_fields.append('time_subscriber')
        self.subscribers_fields.append('latency')

        self.traffic_fields = list()
        self.traffic_fields.append('id')
        self.traffic_fields.append('received')
        self.traffic_fields.append('received_mqtt')
        self.traffic_fields.append('received_coap')

    def show_tables(self):
        self.cursor.execute(self.sql_show_publications)
        result = self.cursor.fetchone()
        if not result:
            return False

        self.cursor.execute(self.sql_show_subscriptions)
        result = self.cursor.fetchone()
        if not result:
            return False

        self.cursor.execute(self.sql_show_traffic)
        result = self.cursor.fetchone()
        if not result:
            return False

        return True

    def drop(self):
        try:
            self.cursor.execute(self.sql_drop_publications)
        except:
            pass

        try:
            self.cursor.execute(self.sql_drop_subscriptions)
        except:
            pass

        try:
            self.cursor.execute(self.sql_drop_traffic)
        except:
            pass

    def vacuum(self):
        self.truncate_publications()
        self.truncate_subscriptions()
        self.truncate_traffic()
    
    def truncate_publications(self):
        self.cursor.execute(self.sql_truncate_publications)
        
    def truncate_subscriptions(self):
        self.cursor.execute(self.sql_truncate_subscriptions)
        
    def truncate_traffic(self):
        self.cursor.execute(self.sql_truncate_traffic)
    
    def get_count_publications(self):
        self.cursor.execute(self.sql_get_count_publications)
        for data in self.cursor:
            return data[0]

    def get_count_subscriptions(self):
        self.cursor.execute(self.sql_get_count_subscriptions)
        for data in self.cursor:
            return data[0]

    def get_count_traffic(self):
        self.cursor.execute(self.sql_get_count_traffic)
        for data in self.cursor:
            return data[0]

    def save_publication(self, data):
        self.cursor.execute(self.sql_save_publication, data)
        self.cnx.commit()

    def save_subscription(self, data):
        self.cursor.execute(self.sql_save_subscription, data)
        self.cnx.commit()

    def get_all_publications(self):
        self.cursor.execute(self.sql_get_all_publications)
        data = list()

        for lista in self.cursor:
            row = dict()
            index = 0

            for i in lista:
                row[self.publisher_fields[index]] = i
                index = index + 1
            data.append(row)

        return data

    def get_all_subscriptions(self):
        self.cursor.execute(self.sql_get_all_suscriptions)
        data = list()

        for lista in self.cursor:
            row = dict()
            index = 0

            for i in lista:
                row[self.subscribers_fields[index]] = i
                index = index + 1
            data.append(row)

        return data

    def get_traffic(self):
        self.cursor.execute(self.sql_get_all_traffic)
        data = list()

        for lista in self.cursor:
            row = dict()
            index = 0

            for i in lista:
                row[self.traffic_fields[index]] = i
                index = index + 1
            data.append(row)

        return data

    def get_all(self):
        return self.get_all_publications(), self.get_all_subscriptions()

    def close(self):
        if self.cnx:
            self.cnx.close()

