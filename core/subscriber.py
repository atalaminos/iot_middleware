#!/usr/bin/env python
# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import time
import uuid
import os
import socket
from multiprocessing import Process
import sys
from datetime import datetime
from pynput.keyboard import Key, Listener
from coapthon.client.helperclient import HelperClient

# Configuración
MQTTSubscribers = 5
COAPSubscribers = 5

# Variables globales
globals()['exit'] = False

# Manejo de teclas
def on_release(key):
    if key == Key.esc:
        globals()['exit'] = True
        return False


class MQTTSubscriber:

    name_topic = "topic"
    broker_address="localhost"
    port = 9090
    #broker_address="11.0.1.3"
    #port = 1883
    count = 0

    def __init__(self):
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message     
        self.client.connect(self.broker_address, self.port, 60)
        self.client.loop_forever()

    def getMsg(self,msg):
        tmp = map(str, filter(None, msg.split(":")))
        return tmp[0], tmp[1], tmp[2]


    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))

        # Subscribing in on_connect() means that if we lose the connection and
        # reconnect then subscriptions will be renewed.
        self.client.subscribe(self.name_topic)

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        ip, pid, cad = self.getMsg(msg.payload)
        self.count = self.count + 1
        print (self.count)


def MQTTworker(num):
    mqtt = MQTTSubscriber()

            
            
def COAPworker(num):
    coap = COAPSubscriber()

    
if __name__ == '__main__':

    processes = []
    for i in range(MQTTSubscribers):
    
        p = Process(target=MQTTworker, args=('bob',))
        p.start()
        p.join()
        processes.append(p)
        print "Lanzado suscriptor MQTT " + i
        
    processes = []
    for i in range(COAPSubscribers):
        p = Process(target=COAPworker, args=('bob',))
        p.start()
        p.join()
        processes.append(p)
        print "Lanzado suscriptor COAP " + i
        
   
      
    with Listener(on_release=on_release) as listener:
        listener.join()
