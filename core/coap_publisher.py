#!/usr/bin/env python
# -*- coding: utf-8 -*-

from coapthon.client.helperclient import HelperClient
from base import Base
import threading


class CoapPublisher(Base):
    
    protocol = "COAP"
    type = "Publisher"
    count = 1
    
    def __init__(self, process_id, shared, conf, out):
        Base.__init__(self, process_id, shared, conf, out, self.protocol, self.type)

    def start(self):
        try:
            self.client = HelperClient(server=(self.conf['coap_broker_address'], self.conf['coap_broker_port']))
        except:
            self.out.put("COAP broker failed")
            return False

        return True
    
    def publish(self):
    
       # Generar mensaje
        msg = self.generate_msg(self.count)
        
        # Publicar
        self.client.put(self.conf['topic_name'], msg)
        
        # Insertar en la cola para actualizar base de datos
        now = self.utils.now()
        self.dict_msg[now] = msg
        
        # Aumentar contador
        self.count = self.count + 1

        self.out.put("COAP Publisher (" + self.id + ") -> "  + str(msg))

                
    def close(self):
        if self.client:
            self.client.stop()

