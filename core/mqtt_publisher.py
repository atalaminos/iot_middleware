#!/usr/bin/env python
# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
from base import Base


class MqttPublisher(Base):

    protocol = "MQTT"
    type = "Publisher"
    count = 1
    
    def __init__(self, process_id, shared, conf, out):
        Base.__init__(self, process_id, shared, conf, out, self.protocol, self.type)

    def start(self):
        try:
            self.client = mqtt.Client()
            self.client.connect(self.conf['mqtt_broker_address'], self.conf['mqtt_broker_port'])
        except:
            self.out.put("MQTT broker failed")
            return False

        return True

    def publish(self):
    
        # Generar mensaje
        msg = self.generate_msg(self.count)
        
        # Publicar
        self.client.publish(self.conf['topic_name'], msg, qos=self.conf['mqtt_qos'])
        
        # Insertar en la cola para actualizar base de datos
        now = self.utils.now()
        self.dict_msg[now] = msg
        
        # Aumentar contador
        self.count = self.count + 1
        
        self.out.put("MQTT Publisher (" + self.id + ") -> "  + str(msg))

        
    def close(self):
        if self.client:
            self.client.disconnect()


