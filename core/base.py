#!/usr/bin/env python
# -*- coding: utf-8 -*-

import uuid
import os
import socket
import time
import threading
import collections
from database import Mysql
from utils import Utils


        
class Base():

    # Connection options
    shared = None
    process_id = None
    conf = None
    out = None
    client = None
    count = 0
    msg_base = None

    # Database
    mysql = None
    mysql_exit = None
    dict_msg = None

    # Utils
    utils = None

    # Id
    id_list = None
    id = None

    # Protocol and type
    protocol = None
    type = None

    def __init__(self, process_id, shared, conf, out, protocol, type):

        self.process_id = process_id
        self.shared = shared
        self.conf = conf
        self.out = out
        self.protocol = protocol
        self.type = type
        
        self.utils = Utils()
        self.dict_msg = collections.OrderedDict()

        self.id_list = list()
        self.id_list.append(socket.gethostname())
        self.id_list.append(os.getpid())
        self.id_list.append(str(process_id))
        self.id_list.append(str(protocol))

        self.id = socket.gethostname() + ":" + str(os.getpid()) + ":" + str(process_id) + ":" + str(protocol)

        # Database connection
        self.mysql = Mysql(conf['mysql_host'], conf['mysql_user'], conf['mysql_password'], conf['mysql_database'])
        self.mysql_exit = False

        # Msg base (IP:PID:PROCESS_ID:PROTOCOL)
        self.msg_base = socket.gethostbyname(socket.gethostname()) + ":" + \
                        str(os.getpid()) + ":" + str(process_id) + ":" + protocol

        threading.Thread(target=self.mysql_thread).start()

    def generate_msg(self, count):
        # COUNT:IP:PID:PROCESS_ID:PROTOCOL:TIME:RANDOM_MSG
        return str(count) + ":" + str(self.msg_base) + ":" + self.utils.now() + ":" + uuid.uuid4().get_hex()[0:6]

    def mysql_thread(self):
    
        while not self.mysql_exit:
        
            while len(self.dict_msg) != 0:
                
                key = list(self.dict_msg.keys())[0]
                msg = self.dict_msg[key]
                
                # Convertir los datos que se reciben a una lista
                data = self.utils.strtok(msg)

                if self.type == "Publisher":

                    # Se recibe lo siguiente: COUNT:IP:PID:PROCESS_ID:PROTOCOL:TIME:RANDOM_MSG

                    # Se añade el msg completo al final de la lista, sobreescribiendo la cadena aleatoria
                    data[-1] = msg
                    self.mysql.save_publication(data)

                elif self.type == "Subscriber":

                    # Se recibe lo siguiente: COUNT:IP:PID:PROCESS_ID:PROTOCOL:TIME:RANDOM_MSG:NOW
                    now = data[-1]
                    latency = float(now) - float(data[5])
                    del data[-1]
                    data[-1] = self.utils.strtok_reverse(data)
                    self.mysql.save_subscription(data + self.id_list + list([float(now), latency]))

                del self.dict_msg[key]

            time.sleep(0.1)

    def wait(self):

        self.out.put(self.protocol + " " + self.type + " (" + self.id + ") -> Waiting database...")

        # El proceso ha terminado de enviar todos los registros a la base de datos
        while len(self.dict_msg) != 0:
            time.sleep(1)

        # Cerrar base de datos
        self.mysql_exit = True
        self.mysql.close()

        self.out.put(self.protocol + " " + self.type + " (" + self.id + ") -> END")

        self.shared[self.process_id] = 1



