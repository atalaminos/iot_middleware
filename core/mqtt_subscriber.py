#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import threading
import paho.mqtt.client as mqtt
from base import Base
import uuid

class MqttSubscriber(Base):

    protocol = "MQTT"
    type = "Subscriber"
    count = 0
    exit = False
    received = False
    index_publishers = None
    delay = None

    def __init__(self, process_id, shared, conf, index_publishers, delay, out):
        Base.__init__(self, process_id, shared, conf, out, self.protocol, self.type)
        self.index_publishers = index_publishers
        self.delay = delay

    def start(self):
        try:
            self.client = mqtt.Client()
            self.client.on_message = self.on_message
            self.client.on_connect = self.on_connect
            self.client.connect(self.conf['mqtt_broker_address'], self.conf['mqtt_broker_port'])
        except:
            self.out.put("MQTT broker failed")
            return False

        return True

    def on_connect(self, client, userdata, flags, rc):
        self.client.subscribe(self.conf['topic_name'],  qos=self.conf['mqtt_qos'])

    def on_message(self, client, userdata, msg):

        self.received = True
        now = self.utils.now()
        self.dict_msg[str(self.count)] = msg.payload + ":"  + str(now)
        self.out.put("MQTT Subscriber (" + str(self.count) + ":"+ self.id + " -> "  + str(msg.payload) + " (" + str(now) + ")")
        self.count += 1

    def wait_publishers(self):

        # Espera hasta que terminen todos los publicadores
        exit = False
        while not exit:

            exit = True
            for i in range(0, len(self.delay) - 1):
                if (self.utils.now_int() - self.delay[self.process_id - self.index_publishers]) < 1000:
                    exit = False

            for i in range(0, self.index_publishers - 1):
                if self.shared[i] == 0:
                    exit = False

            time.sleep(5)

        self.out.put("MQTT Subscriber (" + self.id + ") -> " + str(self.count) + " samples received")
        self.client.disconnect()
        self.exit = True

    def subscribe(self):
        if self.client:
            threading.Thread(target=self.wait_publishers).start()
            self.client.loop_forever()
    
    def close(self):
        if self.client:
            self.client.disconnect()



