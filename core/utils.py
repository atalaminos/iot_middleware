#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import math

class Utils():

    def now(self):
        millis = int(round(time.time() * 1000))
        return str(millis)

    def now_int(self):
        millis = int(round(time.time() * 1000))
        temp = str(millis)
        return int(temp[4:])

    def strtok(self, msg):
        return [str(x) for x in msg.split(':') if msg.strip()]

    def strtok_reverse(self, data):
        msg = ''
        for field in data:
            msg += field + ":"
        msg = msg[:-1]
        return msg
        
    def mean(self, data):

        if len(data) == 0:
            return -1

        return sum(data) / float(len(data))

    def pstdev(self, data):

        if len(data) == 0:
            return -1

        mean = self.mean(data)

        sum = 0.0
        for i in range(0, len(data)):
            sum = sum + math.pow((data[i] - mean), 2.0)

        return math.sqrt(1.0/(float(len(data))-1.0) * sum)

    def convert_list_to_dict(self, suscriptions_data):

        result = dict()

        for row in suscriptions_data:

            msg = row['msg']

            if msg not in result:
                result[msg] = list()

            result[msg].append(row)
        
        return result




