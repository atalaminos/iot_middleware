#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import threading
from coapthon.client.helperclient import HelperClient
from base import Base
from core.utils import Utils


class CoapSubscriber(Base):

    protocol = "COAP"
    type = "Subscriber"
    exit = False
    index_publishers = None
    delay = None

    def __init__(self, process_id, shared, conf, index_publishers, delay, out):
        Base.__init__(self, process_id, shared, conf, out, self.protocol, self.type)
        self.index_publishers = index_publishers
        self.delay = delay

    def start(self):
        try:
            self.client = HelperClient(server=(self.conf['coap_broker_address'], self.conf['coap_broker_port']))
        except:
            self.out.put("COAP broker failed")
            return False

        return True

    def on_message(self, response):
        self.delay[self.process_id - self.index_publishers] = self.utils.now_int()


        try :
            if ':' not in response.payload:
                return
        except:
            return

        now = self.utils.now()
        self.dict_msg[str(self.count)] = response.payload + ":"  + str(now)
        self.out.put("COAP Subscriber (" + str(self.count) + ":" + self.id + ") -> "  + str(response.payload))
        self.count += 1

    def wait_publishers(self):

        # Espera hasta que terminen todos los publicadores
        exit = False
        while not exit:

            exit = True
            for i in range(0, len(self.delay) - 1):
                if (self.utils.now_int() - self.delay[self.process_id-self.index_publishers]) < 1000:
                    exit = False

            for i in range(0, self.index_publishers ):
                if self.shared[i] == 0:
                    exit = False

            time.sleep(5)

        self.out.put("COAP Subscriber (" + self.id + ") -> " + str(self.count) + " samples received")
        self.client.stop()
        self.exit = True

    def subscribe(self):
        if self.client:
            threading.Thread(target=self.wait_publishers).start()

            self.client.observe(self.conf['topic_name'], self.on_message)
            while not self.exit: 
                time.sleep(0.5)
