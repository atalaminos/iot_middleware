#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import threading
import time
import os
import multiprocessing
from core.mqtt_publisher import MqttPublisher
from core.coap_publisher import CoapPublisher
from core.mqtt_subscriber import MqttSubscriber
from core.coap_subscriber import CoapSubscriber
from core.database import Mysql
from pynput.keyboard import Key, Listener
from coapthon.client.helperclient import HelperClient

COMMAND_CLEAR_SCREEN = "cls"

# Configuration
conf = dict()
conf['mqtt_qos'] = 1  # 0 -> not acknowledgment, 1 -> acknowledgment
conf['publications'] = 3
conf['publisher_interval'] = 0.1
conf['mqtt_publishers'] = 0
conf['coap_publishers'] = 45
conf['mqtt_subscribers'] = 0
conf['coap_subscribers'] = 45
# conf['mqtt_broker_address'] = "11.0.1.3"
# conf['mqtt_broker_port'] = 1883
# conf['coap_broker_address'] = "127.0.0.1"
# conf['coap_broker_port'] = 9091
conf['mqtt_broker_address'] = "11.0.1.3"
conf['mqtt_broker_port'] = 1883
conf['coap_broker_address'] = "11.0.1.3"
conf['coap_broker_port'] = 5683
conf['topic_name'] = "topic"
conf['mysql_host'] = "localhost"
conf['mysql_user'] = "iot"
conf['mysql_password'] = "iot"
conf['mysql_database'] = "iot"
conf['print'] = True

globals()['exit'] = False


# Keys handler
def on_release(key):
    if key == Key.esc:
        globals()['exit'] = True
        return False


def mqtt_subscriber_worker(process_id, shared, index_publishers, delay, out):
    mqtt = MqttSubscriber(process_id, shared, conf, index_publishers, delay, out)
    if not mqtt.start():
        mqtt.close()
        sys.exit(1)

    mqtt.subscribe()
    mqtt.wait()


def mqtt_publisher_worker(process_id, shared, out):

    # Esperanndo a que se conecten los suscriptores
    time.sleep(5)

    mqtt = MqttPublisher(process_id, shared, conf, out)
    if not mqtt.start():
        mqtt.close()
        sys.exit(1)

    for i in range(0, conf['publications']):
        time.sleep(conf['publisher_interval'])
        mqtt.publish()

    time.sleep(2)
    mqtt.close()
    mqtt.wait()


def coap_subscriber_worker(process_id, shared, index_publishers, delay, out):
    coap = CoapSubscriber(process_id, shared, conf, index_publishers, delay, out)
    if not coap.start():
        coap.close()
        sys.exit(1)

    coap.subscribe()
    coap.wait()


def coap_publisher_worker(process_id, shared, out):

    # Esperanndo a que se conecten los suscriptores
    time.sleep(5)

    coap = CoapPublisher(process_id, shared, conf, out)
    if not coap.start():
        coap.close()
        sys.exit()

    time.sleep(5)
    for i in range(0, conf['publications']):
        time.sleep(conf['publisher_interval'])
        coap.publish()

    time.sleep(5)
    coap.close()
    coap.wait()


def killAll():
    for process in processes:
        process.terminate()


def watchdog(listener, shared):
    exit = False
    while not exit:
        exit = True
        for i in range(0, len(shared)):
            #print str(i) + ":" + str(shared[i])
            if shared[i] == 0:
                exit = False
        time.sleep(1)

    killAll()
    listener.stop()


def handle_keys(shared):
    with Listener(on_release=on_release) as listener:
        threading.Thread(target=watchdog, args=(listener,shared)).start()
        listener.join()
    print (os._exit(1))


def printf(out):
    while not globals()['exit']:
        result = out.get()
        if conf['print']:
            print (result)


if __name__ == '__main__':

    # Procesos
    processes = []
    processes_id_count = 0

    # Variable compartida para publicadores y suscriptores
    processes_num = conf['mqtt_publishers'] + conf['coap_publishers'] + conf['mqtt_subscribers'] + conf[
        'coap_subscribers']
    shared = multiprocessing.Array('i', processes_num)
    out = multiprocessing.Queue()
    for i in range(0, processes_num - 1):
        shared[i] = 0

    delay_num = conf['mqtt_subscribers'] + conf[
        'coap_subscribers']
    delay = multiprocessing.Array('i', delay_num)
    for i in range(0, delay_num - 1):
        delay[i] = 0

    # Keys handle
    threading.Thread(target=handle_keys, args=(shared,)).start()

    # os.system(COMMAND_CLEAR_SCREEN)

    # Database connection
    mysql = Mysql(conf['mysql_host'], conf['mysql_user'], conf['mysql_password'], conf['mysql_database'])

    # Truncate all tables
    print ('Cleaning database')
    mysql.vacuum()
    print ('Done')

    threading.Thread(target=printf, args=(out,)).start()

    # Clean COAP topic
    if conf['coap_subscribers'] != 0 or conf['coap_publishers'] != 0:
        client = HelperClient(server=(conf['coap_broker_address'], conf['coap_broker_port']))
        client.put(conf['topic_name'], "")

    # Publishers
    processes_id_count = 0
    for i in range(conf['mqtt_publishers']):
        print ('Launching MQTT publisher ' + str(i + 1))
        p = multiprocessing.Process(target=mqtt_publisher_worker, args=(processes_id_count, shared, out,))
        p.start()
        processes.append(p)
        processes_id_count += 1

    for i in range(conf['coap_publishers']):
        print ('Launching COAP publisher ' + str(i + 1))
        p = multiprocessing.Process(target=coap_publisher_worker, args=(processes_id_count, shared, out,))
        p.start()
        processes.append(p)
        processes_id_count += 1


    index_publishers = processes_id_count

    # Subscribers
    print ('Creating MQTT subscribers')
    for i in range(conf['mqtt_subscribers']):
        print ('Launching MQTT subscriber ' + str(i + 1))
        p = multiprocessing.Process(target=mqtt_subscriber_worker, args=(processes_id_count, shared, index_publishers, delay, out,))
        p.start()
        processes.append(p)
        processes_id_count += 1

    for i in range(conf['coap_subscribers']):
        print ('Launching COAP subscriber ' + str(i + 1))
        p = multiprocessing.Process(target=coap_subscriber_worker, args=(processes_id_count, shared, index_publishers, delay, out,))
        p.start()
        processes.append(p)
        processes_id_count += 1

    time.sleep(5)




