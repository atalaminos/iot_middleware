#!/usr/bin/env python
# -*- coding: utf-8 -*-

from coapthon.client.helperclient import HelperClient
import time


def on_message(response):
    print response

client = HelperClient(server=("127.0.0.1", 5883))
client.observe("topic", on_message)

while True:
    time.sleep(0.5)