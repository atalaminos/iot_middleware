#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from socket import *
from threading import Thread
import time
import collections
from pynput.keyboard import Key, Listener
from core.database import Mysql
import threading
import os
import multiprocessing
LOGGING = 1


conf = dict()
conf['mysql_host'] = "localhost"
conf['mysql_user'] = "iot"
conf['mysql_password'] = "iot"
conf['mysql_database'] = "iot"
conf['print'] = True


def printf(out):
    while True:
        result = out.get()
        if conf['print']:
            print result
        time.sleep(0.05)
            
def on_release(key):
    if key == Key.esc:
        os._exit(1)
        
        
def handle_keys(shared):
    with Listener(on_release=on_release) as listener:
        listener.join()
        
def log( s ):
    if LOGGING:
        print '%s:%s' % ( time.ctime(), s )
        sys.stdout.flush()

class PipeThread( Thread ):

    mysql = None
    mysql_exit = None
    pipes = []
    traffic = None
    update_traffic = False
    
    def toBit(self, s):
        result = []
        for c in s:
            bits = bin(ord(c))[2:]
            bits = '00000000'[len(bits):] + bits
            result.extend([int(b) for b in bits])
        return result

    def toInt(self, s):
        result = 0
        for bit in s:
            result = (result << 1) | bit
        return result
        
    def frombits(self, bits):
        chars = []
        for b in range(len(bits) / 8):
            byte = bits[b*8:(b+1)*8]
            chars.append(chr(int(''.join([str(bit) for bit in byte]), 2)))
        return ''.join(chars)
        
            
    class Forward:
        def __init__(self):
            self.forward = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        def start(self, host, port):
            try:
                self.forward.connect((host, port))
                return self.forward
            except Exception, e:
                print e
                return False
                
            
    def __init__( self, source, sink ):
        Thread.__init__( self )
        self.source = source
        self.sink = sink
        
        # Database connection
        self.mysql = Mysql(conf['mysql_host'], conf['mysql_user'], conf['mysql_password'], conf['mysql_database'])
        self.mysql_exit = False
        
        traffic = collections.OrderedDict()
        traffic['received'] = 0
        traffic['received_mqtt'] = 0
        traffic['received_coap'] = 0
        
        #log( 'Creating new pipe thread  %s ( %s -> %s )' % \
        #    ( self, source.getpeername(), sink.getpeername() ))
        PipeThread.pipes.append( self )
        #log( '%s pipes active' % len( PipeThread.pipes ))

    def mysql_thread(self):
        while not self.mysql_exit:
            while self.update_traffic:
                self.mysql.truncate_traffic()
                #self.mysql.save_traffic(list(data))
                self.update_traffic = False
            time.sleep(0.5)       
        
    def process(self, data, out):
        
        if not data: 
        
            return
                
        if True:
            topic_length_bits = self.toBit(data[2:4])
            topic_length = self.toInt(topic_length_bits)
            topic_name_bits = self.toBit(data[4:4+topic_length])
            topic_name = self.frombits(topic_name_bits)
            topic_body = data[4+topic_length:len(data)]
            
            
            if topic_length == 5:
            
                msg = ('MQTT Packet!')
                msg += ('\nTopic length: ' + str(topic_length))
                msg += ('\nTopic name: ' + str(topic_name))
                msg += ('\nBody: ' + topic_body)
                msg += ('\n-------------------')
                
                out.put(msg)
            
            #received = 20
            #received_mqtt = 20
            #update_traffic = True
        
        elif data[4:8] == "MQTT":
            mqtt = True
            
        #if coap:
        
            #received = 20
            #received_coap = 20
            #update_traffic = True
        
        
        
        # Actualizar datos del tráfico
        #if update_traffic:
            #traffic['received'] += received
            #traffic['received_mqtt']+= received_mqtt
            #traffic['received_coap'] += received_coap
            #self.update_traffic = True

    def run(self):

        print 'a'
        mqtt = False
        coap = False
        update_traffic = False

        shared = multiprocessing.Array('i', 1)
        shared[0] = 0

        threading.Thread(target=handle_keys, args=(shared,)).start()
        threading.Thread(target=self.mysql_thread).start()
        
        out = multiprocessing.Queue()
        p = multiprocessing.Process(target=printf, args=(out,))
        p.start()

        while shared[0] == 0:
            try:
            
                received = 0
                received_mqtt = 0
                received_coap = 0
                
                data = self.source.recv(1024)    
                data_temp = data
                threading.Thread(target=self.process, args=(data_temp,out)).start()
                      
                # Enviar
                print 'a'
                self.sink.send( data )
               
            except:
                break

        #log( '%s terminating' % self )
        PipeThread.pipes.remove( self )
        #log( '%s pipes active' % len( PipeThread.pipes ))
        
class Pinhole( Thread ):
    def __init__( self, port, newhost, newport ):
        Thread.__init__( self )
        self.newhost = newhost
        self.newport = newport
        self.sock = socket( AF_INET, SOCK_STREAM )
        self.sock.bind(( '', port ))
        self.sock.listen(5)
    
    def run( self ):
        while 1:
            newsock, address = self.sock.accept()
            fwd = socket(AF_INET, SOCK_STREAM )
            fwd.connect((self.newhost, self.newport ))
            PipeThread(newsock, fwd).start()
            PipeThread(fwd, newsock).start()
       
if __name__ == '__main__':

    print 'Starting Pinhole'
    import sys

    if len( sys.argv ) > 1:
        port = newport = int( sys.argv[1] )
        newhost = sys.argv[2]
        if len( sys.argv ) == 4: newport = int( sys.argv[3] )
        Pinhole( port, newhost, newport ).start()
    else:
        #Pinhole( 9090, '11.0.1.3', 1883 ).start()
        Pinhole(9090, '11.0.1.1', 5683).start()