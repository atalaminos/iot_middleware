#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Configuration
from core.database import Mysql
from core.manager import Manager
import os
import sys


COMMAND_CLEAR_SCREEN = "cls"

conf = dict()
conf['mysql_host'] = "localhost"
conf['mysql_user'] = "iot"
conf['mysql_password'] = "iot"
conf['mysql_database'] = "iot"
conf['mysqldump_path'] = 'C:\\xampp\\mysql\\bin\\mysqldump.exe'
conf['mysql_path'] = 'C:\\xampp\\mysql\\bin\\mysql.exe'
conf['backup_path'] = 'data\\'

class Menu():

    menu = None
    test_name = None
    mysql = None
    manager = None

    def __init__(self):

        # Manager
        self.manager = Manager()

        self.menu = dict()
        os.system(COMMAND_CLEAR_SCREEN)
        self.menu[0] = "Write test name"
        self.menu[1] = "Truncate all"
        self.menu[2] = "Publishers info"
        self.menu[3] = "Subscribers info"
        self.menu[4] = "Latency and jitter"
        self.menu[5] = "Lost samples"
        self.menu[6] = "Traffic"
        self.menu[7] = "Print all"
        self.menu[8] = "Save sql"
        self.menu[9] = "Restore sql"
        self.menu[10] = "List sql"
        self.menu['e'] = "Exit"

    def write_name(self):
        self.test_name = raw_input("Write name: ")

    def publishers_info(self):
        print ('Getting publishers info...')
        data = self.mysql.get_all_publications()
        info = self.manager.get_publishers_info(data)
        os.system(COMMAND_CLEAR_SCREEN)

        print "Total of publishers: " + str(info['total_publishers'])
        print "Total of MQTT publishers: " + str(info['total_mqtt_publishers'])
        print "Total of COAP publishers: " + str(info['total_coap_publishers'])
        print "Total of samples published: " + str(info['total_samples_sent'])
        print "Total of MQTT samples published: " + str(info['total_mqtt_samples_sent'])
        print "Total of COAP samples published: " + str(info['total_coap_samples_sent'])

    def subscribers_info(self):
        print ('Getting subscribers info...')
        data = self.mysql.get_all_subscriptions()
        info = self.manager.get_subscribers_info(data)
        os.system(COMMAND_CLEAR_SCREEN)

        print "Total of subscribers: " + str(info['total_subscribers'])
        print "Total of MQTT subscribers: " + str(info['total_mqtt_subscribers'])
        print "Total of COAP subscribers: " + str(info['total_coap_subscribers'])
        print "Total of samples received: " + str(info['total_samples_received'])
        print "Total of MQTT samples received: " + str(info['total_mqtt_samples_received'])
        print "Total of COAP samples received: " + str(info['total_coap_samples_received'])
        print "Total of MQTT samples received by MQTT subscribers: " + str(
            info['total_mqtt_samples_received_by_mqtt_subscribers'])
        print "Total of MQTT samples received by COAP subscribers: " + str(
            info['total_mqtt_samples_received_by_coap_subscribers'])
        print "Total of COAP samples received by COAP subscribers: " + str(
            info['total_coap_samples_received_by_coap_subscribers'])
        print "Total of COAP samples received by MQTT subscribers: " + str(
            info['total_coap_samples_received_by_mqtt_subscribers'])

    def latency_jitter(self):
        print ('Getting latency and jitter...')
        suscriptions_data = self.mysql.get_all_subscriptions()
        info = self.manager.latency_jitter(suscriptions_data)
        os.system(COMMAND_CLEAR_SCREEN)

        print "Mean latency: " + str(info['total_mean_latency'] / 1000.0)
        print "Min latency: " + str(info['min_latency'] / 1000.0)
        print "Max latency: " + str(info['max_latency'] / 1000.0)
        print "Jitter: " + str(info['jitter'] / 1000.0)

        print "Mean latency of MQTT subscribers: " + str(info['mean_latency_mqtt'] / 1000.0)
        print "Min latency of MQTT subscribers: " + str(info['min_latency_mqtt'] / 1000.0)
        print "Max latency of MQTT subscribers: " + str(info['max_latency_mqtt'] / 1000.0)
        print "Jitter of MQTT subscribers: " + str(info['jitter_mqtt'] / 1000.0)

        print "Mean latency of COAP subscribers: " + str(info['mean_latency_coap'] / 1000.0)
        print "Min latency of COAP subscribers: " + str(info['min_latency_coap'] / 1000.0)
        print "Max latency of COAP subscribers: " + str(info['max_latency_coap'] / 1000.0)
        print "Jitter of COAP subscribers: " + str(info['jitter_coap'] / 1000.0)

        print "Mean latency of MQTT subscribers received from MQTT publishers: " + str(
            info['mean_latency_mqtt_from_mqtt'] / 1000.0)
        print "Min latency of MQTT subscribers received from MQTT publishers: " + str(
            info['min_latency_mqtt_from_mqtt'] / 1000.0)
        print "Max latency of MQTT subscribers received from MQTT publishers: " + str(
            info['max_latency_mqtt_from_mqtt'] / 1000.0)
        print "Jitter of MQTT subscribers received from MQTT publishers: " + str(
            info['jitter_latency_mqtt_from_mqtt'] / 1000.0)

        print "Mean latency of MQTT subscribers received from COAP publishers: " + str(
            info['mean_latency_mqtt_from_coap'] / 1000.0)
        print "Min latency of MQTT subscribers received from COAP publishers: " + str(
            info['min_latency_mqtt_from_coap'] / 1000.0)
        print "Max latency of MQTT subscribers received from COAP publishers: " + str(
            info['max_latency_mqtt_from_coap'] / 1000.0)
        print "Jitter of MQTT subscribers received from COAP publishers: " + str(
            info['jitter_latency_mqtt_from_coap'] / 1000.0)

        print "Mean latency of COAP subscribers received from COAP publishers: " + str(
            info['mean_latency_coap_from_coap'] / 1000.0)
        print "Min latency of COAP subscribers received from COAP publishers: " + str(
            info['min_latency_coap_from_coap'] / 1000.0)
        print "Max latency of COAP subscribers received from COAP publishers: " + str(
            info['max_latency_coap_from_coap'] / 1000.0)
        print "Jitter of COAP subscribers received from COAP publishers: " + str(
            info['jitter_latency_coap_from_coap'] / 1000.0)

        print "Mean latency of COAP subscribers received from MQTT publishers: " + str(
            info['mean_latency_coap_from_mqtt'] / 1000.0)
        print "Min latency of COAP subscribers received from MQTT publishers: " + str(
            info['min_latency_coap_from_mqtt'] / 1000.0)
        print "Max latency of COAP subscribers received from MQTT publishers: " + str(
            info['max_latency_coap_from_mqtt'] / 1000.0)
        print "Jitter of COAP subscribers received from MQTT publishers: " + str(
            info['jitter_latency_coap_from_mqtt'] / 1000.0)

    def lost_samples(self):

        print ('Getting lost samples...')
        publications_data, suscriptions_data = self.mysql.get_all()
        info = self.manager.lost_samples(publications_data, suscriptions_data)
        os.system(COMMAND_CLEAR_SCREEN)

        print "Lost samples: " + str(info['lost_samples'])
        print "Lost samples of MQTT subscribers: " + str(info['lost_samples_mqtt'])
        print "Lost samples of COAP subscribers: " + str(info['lost_samples_coap'])
        print "Lost samples of MQTT subscribers received from MQTT publishers: " + str(
            info['lost_samples_mqtt_from_mqtt'])
        print "Lost samples of MQTT subscribers received from COAP publishers: " + str(
            info['lost_samples_mqtt_from_coap'])
        print "Lost samples of COAP subscribers received from COAP publishers: " + str(
            info['lost_samples_coap_from_coap'])
        print "Lost samples of COAP subscribers received from MQTT publishers: " + str(
            info['lost_samples_coap_from_mqtt'])

    def traffic(self):

        print ('Getting traffic...')
        info = self.mysql.get_traffic()
        os.system(COMMAND_CLEAR_SCREEN)

        if len(info) == 0:
            print 'No traffic'
            return

        print "Traffic: " + str(info[0]['traffic'])
        print "Sent traffic by publishers: " + str(info[0]['sent'])
        print "Received traffic by subscribers: " + str(info[0]['received'])
        print "Sent traffic by MQTT publishers: " + str(info[0]['sent_mqtt'])
        print "Sent traffic by COAP publishers: " + str(info[0]['sent_coap'])
        print "Received traffic by MQTT subscribers: " + str(info[0]['received_mqtt'])
        print "Received traffic by COAP subscribers: " + str(info[0]['received_coap'])
        print "Received traffic by MQTT subscribers from MQTT publishers: " + str(info[0]['received_mqtt_from_mqtt'])
        print "Received traffic by MQTT subscribers from COAP publishers: " + str(info[0]['received_mqtt_from_coap'])
        print "Received traffic by COAP subscribers from COAP publishers: " + str(info[0]['received_coap_from_coap'])
        print "Received traffic by COAP subscribers from MQTT publishers: " + str(info[0]['received_coap_from_mqtt'])


    def print_all(self):
        self.publishers_info()
        self.subscribers_info()
        self.latency_jitter()
        self.lost_samples()
        self.traffic()

    def save_sql(self):
        os.system(COMMAND_CLEAR_SCREEN)

        if self.test_name is None:
            self.test_name = raw_input("Name: ")

        print('Performing backup...')
        sentence = conf['mysqldump_path'] + " --user " + conf['mysql_user'] + " --password=" + conf['mysql_password'] + " -h " +\
                   conf['mysql_host'] + " --databases " + conf['mysql_database'] + " > " + conf['backup_path'] + self.test_name + ".sql"
        os.system(sentence)

        print('Done')
        raw_input()

    def restore_sql(self):

        os.system(COMMAND_CLEAR_SCREEN)
        test_name = raw_input("Name: ")

        if os.path.isfile(conf['backup_path'] + test_name + ".sql"):
            self.test_name = test_name
            self.mysql.drop()

            print('Restoring backup...')
            sentence = conf['mysql_path'] + " --user " + conf['mysql_user'] + " --password=" + conf[
                'mysql_password'] + " -h " + conf['mysql_host'] + " " + conf['mysql_database']\
                       + " < " + conf['backup_path'] + self.test_name + ".sql"
            os.system(sentence)

            print('Done')

        else:
            print ('SQL file does not exist')

        raw_input()

    def list_sql(self):
        os.system('cls')
        list = os.listdir(conf['backup_path'])

        if len(list) == 0:
            print 'No backups'

        else:
            for file in os.listdir(conf['backup_path']):
                print file[:-4]

        raw_input()

    def start(self):

        while True:

            # Database connection
            self.mysql = Mysql(conf['mysql_host'], conf['mysql_user'], conf['mysql_password'], conf['mysql_database'])

            if not self.mysql.show_tables():
                print 'Creating database'
                self.mysql.drop()
                sentence = conf['mysql_path'] + " --user " + conf['mysql_user'] + " --password=" + conf[
                    'mysql_password'] + " -h " + conf['mysql_host'] + " " + conf['mysql_database'] \
                           + " < " + "scheme.sql"
                print sentence
                os.system(sentence)

            try :
                publications_rows = self.mysql.get_count_publications()
                subscriptions_rows = self.mysql.get_count_subscriptions()
                traffic_rows = self.mysql.get_count_traffic()
            except:
                print 'Error connecting database'
                sys.exit(1)

            os.system(COMMAND_CLEAR_SCREEN)
            if self.test_name is None:
                print ("Test name is not defined")
            else:
                print ("Test name: " + self.test_name)

            print "---------"
            print "Publications rows: " + str(publications_rows)
            print "Subscriptions rows: " + str(subscriptions_rows)
            print "Traffic rows: " + str(traffic_rows)
            print "---------"

            options = self.menu.keys()
            options.sort()
            for entry in options:
                print entry, "-", self.menu[entry]

            selection = raw_input("Please Select: ")

            os.system('cls')
            if selection == '0':
                self.write_name()

            elif selection == '1':
                print ('Performing vacuum...')
                self.mysql.vacuum()
                print ('Done')
                raw_input()

            elif selection == '2':
                self.publishers_info()
                raw_input()

            elif selection == '3':
                self.subscribers_info()
                raw_input()

            elif selection == '4':
                self.latency_jitter()
                raw_input()

            elif selection == '5':
                self.lost_samples()
                raw_input()

            elif selection == '6':
                self.traffic()
                raw_input()

            elif selection == '7':
                self.publishers_info()
                print '-------------------'
                self.subscribers_info()
                print '-------------------'
                self.latency_jitter()
                print '-------------------'
                self.lost_samples()
                raw_input()

            elif selection == '8':
                self.save_sql()

            elif selection == '9':
                self.restore_sql()

            elif selection == '10':
                self.list_sql()

            elif selection == 'e':
                self.mysql.close()
                break

            else:
                print "Unknown option selected"

            os.system('cls')


m = Menu()
m.start()