#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from socket import *
from threading import Thread
import time
import collections
from core.database import Mysql
import threading
import os
import multiprocessing
import paho.mqtt.client as mqtt

LOGGING = 1
BUFFER_SIZE = 2 ** 10  # 1024. Keep buffer size as power of 2.

conf = dict()
conf['mysql_host'] = "localhost"
conf['mysql_user'] = "iot"
conf['mysql_password'] = "iot"
conf['mysql_database'] = "iot"
conf['print'] = True
conf['i'] = 7000

def printf(out):
    while True:
        result = out.get()
        if conf['print']:
            print result
        time.sleep(0.05)


def log(s):
    if LOGGING:
        print '%s:%s' % (time.ctime(), s)
        sys.stdout.flush()


class PipeThread(Thread):
    mysql = None
    mysql_exit = None
    pipes = []
    traffic = None
    update_traffic = False

    def toBit(self, s):
        result = []
        for c in s:
            bits = bin(ord(c))[2:]
            bits = '00000000'[len(bits):] + bits
            result.extend([int(b) for b in bits])
        return result

    def toInt(self, s):
        result = 0
        for bit in s:
            result = (result << 1) | bit
        return result

    def frombits(self, bits):
        chars = []
        for b in range(len(bits) / 8):
            byte = bits[b * 8:(b + 1) * 8]
            chars.append(chr(int(''.join([str(bit) for bit in byte]), 2)))
        return ''.join(chars)

    def __init__(self, data, address, sock, i):
        Thread.__init__(self)
        self.data = data
        self.i = i
        self.address = address
        self.sock = sock
        PipeThread.pipes.append(self)


    def run(self):

        sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
        sock.bind(('', self.i))

        sock.sendto(self.data, ("127.0.0.1", 5683))

        while True:

            sock.settimeout(10.0)
            try:
                data, address = sock.recvfrom(BUFFER_SIZE)
            except:
                break
            self.sock.sendto(data, self.address)


        #self.sink.send(data)
        #PipeThread.pipes.remove(self)
        # log( '%s pipes active' % len( PipeThread.pipes ))


class Pinhole(Thread):

    def __init__(self, port, newhost, newport):
        Thread.__init__(self)
        self.newhost = newhost
        self.newport = newport
        self.sock = socket(AF_INET, SOCK_DGRAM)
        self.sock.bind(('', port))
        self.client = mqtt.Client()
        self.client.connect("11.0.1.3", 1883)

    def run(self):
        while 1:
            try:
                data, source = self.sock.recvfrom(BUFFER_SIZE)
                if len(data) > 10:
                    msg = ('COAP Packet!')
                    msg += ('\nTopic name: topic')
                    msg += ('\nBody: ' + data[11:])
                    msg += ('\n-------------------')
                    self.client.publish("topic", data[11:])

            except:
                pass
            conf['i'] = conf['i'] + 1
            PipeThread(data, source, self.sock, conf['i']).start()

if __name__ == '__main__':

    print 'Starting Pinhole'
    import sys

    if len(sys.argv) > 1:
        port = newport = int(sys.argv[1])
        newhost = sys.argv[2]
        if len(sys.argv) == 4: newport = int(sys.argv[3])
        Pinhole(port, newhost, newport).start()
    else:
        # Pinhole( 9090, '11.0.1.3', 1883 ).start()
        Pinhole(9091, '127.0.0.1', 5683).start()